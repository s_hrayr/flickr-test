<?php

namespace App\Http\Controllers;

use App\Flickr;
use Illuminate\Http\Request;

use App\Http\Requests;
use JasonGrimes\Paginator;

class HomeController extends Controller
{
    /**
     * Home page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * Search action, which get keyword and page and return photos from Flickr
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $keyword = $request->get('keyword');
        $page = $request->get('page') ? $request->get('page') : 1;

        // get images from flickr
        $flickr = new Flickr();
        $data = $flickr->getImages($keyword,$page);

        // pagination
        $urlPattern = "?keyword=$keyword&page=(:num)";
        $pagination = $data['pagination'];
        $paginator = new Paginator($pagination['totalItems'], $pagination['perPage'], $pagination['currentPage'], $urlPattern);

        return view('search', ['photos' => $data['photos'], 'paginator' => $paginator]);

    }

    /**
     * Show full-size image
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $url = $request->get('url');

        return view('show', ['image' => $url]);

    }
}
